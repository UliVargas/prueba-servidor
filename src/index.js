const app = require("./app");
const sequelize = require("./database/database");


//Creo las tablas de la base de datos desde que se arranca el servidor, esto ahora crear uan base de datos
sequelize.sync({force: false})
    .then(async () => {
        await app.listen(3001, () => {
            console.log("server on port 3001");
        })
    })
    .catch(e => console.log(e.message))