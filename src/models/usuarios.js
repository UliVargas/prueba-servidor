const { DataTypes } = require("sequelize");
const sequelize = require("../database/database");
const Transacciones = require("./transacciones");

const Usuarios = sequelize.define('usuarios', {
    uid: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    lastname: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phone: {
        type: DataTypes.STRING,
    },
    role: {
        type: DataTypes.STRING,
        defaultValue: "user"
    },
    created: {
        type: DataTypes.DATEONLY,
        defaultValue: DataTypes.NOW
    }
}, {
    timestamps: false
})

Usuarios.hasMany(Transacciones);
Transacciones.belongsTo(Usuarios);

module.exports = Usuarios;