require("dotenv").config();
const jwt = require("jsonwebtoken");
const Usuarios = require("../models/usuarios");

const generateToken = (user) => {
    try {
        return jwt.sign({user: user.uid, role: user.role}, process.env.SECRET);
    } catch (error) {
        return null
    };
};

const verified = async (token) => {
   try {
       return jwt.verify(token, process.env.SECRET);
   } catch (error) {
       return null
   };
};

const validateUser = async (req, res, next) => {
    const token = req.headers.authorization?.split(" ")[1];
    const data = await verified(token);
    
    if(!token) res.status(403).json({msg: "Acceso Denegado"});
    else next();
};

const validateAdmin = (roles) => async (req, res, next) => {
    const token = req.headers.authorization?.split(" ")[1];
    const data = await verified(token);
    const user = await Usuarios.findByPk(data.user);

    if(roles.includes(user.role)) next();
    else res.status(403).json("Acceso Denegado");
};

module.exports = {
    generateToken,
    validateUser,
    validateAdmin
}