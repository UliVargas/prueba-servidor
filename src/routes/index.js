const express = require("express");
const Transacciones = require("../models/transacciones");
const router = express.Router();
const Usuarios = require("../models/usuarios");
const bcrypt = require("bcryptjs");
const { generateToken, validateAdmin, validateUser } = require("../middleware/token");


router.get("/users", validateUser, async (req, res) => {
    const users = await Usuarios.findAll();
    res.status(200).json(users);
});

router.post("/add-user", async (req, res) => {
    const { name, lastname, email, phone, password } = req.body;
    const hash = await bcrypt.hash(password, 10);

    const [user, created] = await Usuarios.findOrCreate({where: { email }, defaults: { password: hash, name, lastname, phone }});
    if(!created) res.status(403).json({msg: "El usuario no pudo ser creado"});
    else res.status(201).json(user);
});

router.get("/transactions", validateUser, validateAdmin(["admin"]), async (req, res) => {
    const transactions = await Transacciones.findAll();
    res.status(200).json(transactions);
});

router.post("/add-transaction", async (req, res) => {
    const {uid, parkingID, total } = req.body;
    const transaction = await Transacciones.create({ parkingID, total, usuarioUid: uid });
    if(!transaction) res.status(403).json({msg: "No se pudo crear la transacción"})
    else res.status(201).json({data: transaction, msg: "El usuario se creó correctamente"})
});

router.post("/login", async (req, res) => {
    const { email, password } = req.body;
    const user = await Usuarios.findOne({where: { email }});

    const verified = await bcrypt.compare(password, user.password)
    if(verified) {
        const token = generateToken(user);
        res.status(200).json({token: token, msg: "Se inició sesión correctamente"});
    } else res.status(403).json({msg: "Credenciales incorrectas"})
});

module.exports = router;