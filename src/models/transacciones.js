const { DataTypes } = require("sequelize");
const sequelize = require("../database/database");

const Transacciones = sequelize.define('transacciones', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    total: {
        type: DataTypes.DECIMAL,
        allowNull: false
    },
    parkingID: {
        type: DataTypes.STRING,
        allowNull: false
    },
    created: {
        type: DataTypes.DATEONLY,
        defaultValue: DataTypes.NOW
    }
}, {
    timestamps: false
})

module.exports = Transacciones;